// Navigation
const ul = document.querySelector('nav ul')
var last_width = document.documentElement.clientWidth

window.onresize = function () {
    let new_width = document.documentElement.clientWidth

    if (new_width <= 960 && last_width >= 960) {
        ul.style.opacity = 0

        setTimeout(() => {
            ul.style.opacity = 1
        }, 1000)
    } else if (new_width >= 960 && last_width <= 960) {
        ul.style.opacity = 1
    }

    last_width = document.documentElement.clientWidth
}

const masonry = new Masonry('.masonry', {
    itemSelector: '.project.show',
    fitWidth: true,
    gutter: 30
})

// Slider
if (document.querySelector('.siema')) {
    const slider = new Siema({
        loop: true,
        easing: 'ease-in-out',
        duration: 1200,
        draggable: false,
        multipleDrag: false
    })

    setInterval(() => slider.next(), 5000)

    document.querySelectorAll('.siema img').forEach(el => {
        el.addEventListener('drag', e => e.preventDefault())
    })
}

const lazy = new LazyLoad({
    elements_selector: '.lazy'
})

const filter = document.querySelector('#filter select')
filter.addEventListener('input', () => {
    const value = filter[filter.selectedIndex].value
    let project;

    if (value === 'all') {
        shown = document.querySelectorAll('.project')
    } else {
        shown = document.querySelectorAll(`.project[data-category="${value}"]`)

        document.querySelectorAll(`.project:not([data-category="${value}"])`).forEach(hide)
    }

    shown.forEach(show)
    reload()
})

function show(project) {
    project.classList.add('show')
    project.style.display = 'block'
    setTimeout(() => {
        project.style.opacity = 1
    }, 200);
}

function hide(project) {
    project.classList.remove('show')
    project.style.opacity = 0
    setTimeout(() => {
        project.style.display = 'none'
    }, 200);
}

function reload() {
    setTimeout(() => {
        masonry.reloadItems()
        masonry.layout()
    }, 200);
}